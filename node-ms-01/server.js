﻿'use strict';

const seneca = require('seneca')();

seneca.add({ role: 'math', cmd: 'add' },
(msg, res) => {
    const sum = msg.left + msg.right;
    res(null, { result: sum });
    });

seneca.add({ role: 'math', cmd: 'multiply' },
    (msg, res) => {
        const product = msg.left * msg.right;
        res(null, { result: product });
    });

seneca.add({ role: 'words', cmd: 'count' },
    (msg, res) => {
        const wordcount = msg.phrase.split(' ').length;
        res(null, { wordcount: wordcount });
    });

seneca.add({ role: 'words', cmd: 'count', skipShort: true},
    (msg, res) => {
        const words = msg.phrase.split(' ');
        let wordcount = 0;

        for (const word of words) {
            if (word.length >= msg.minLength) {
                wordcount++;
            }
        }

        res(null, { wordcount: wordcount });
    });

seneca.act({ role: 'math', cmd: 'add', left: 10, right: 11 }, (error, data) => {
    if (error) {
        return console.error(error);
    }

    return console.log(data);
});

seneca.act({ role: 'math', cmd: 'multiply', left: 10, right: 11 }, (error, data) => {
    if (error) {
        return console.error(error);
    }

    return console.log(data);
});

seneca.act({ role: 'words', cmd: 'count', phrase: 'The quick brown fox jumped over the lazy dog.' }, (error, data) => {
    if (error) {
        return console.error(error);
    }

    return console.log(data);
});

seneca.act({ role: 'words', cmd: 'count', skipShort: true, phrase: 'The quick brown fox jumped over the lazy dog.', minLength: 4}, (error, data) => {
    if (error) {
        return console.error(error);
    }

    return console.log(data);
});